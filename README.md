# Clock

Clock is a simple and easy to use clock for [Ubuntu Touch](https://ubuntu-touch.io/), with time zone support for cities and timer and count down functions.

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/clock.ubports)

<img src="screenshot.png" alt="screenshot" width="200"/>

## Ubuntu Touch

This app is a core app for [Ubuntu Touch](https://ubuntu-touch.io/).
Ubuntu Touch is a mobile OS developed by [UBports](https://ubports.com/), a group of volunteers and passionate people across the world. With Ubuntu Touch we offer a truly unique mobile experience - an alternative to the current most popular operating systems on the market.
We believe that everyone is free to use, study, share and improve all software we create. Whenever possible, everything is distributed under free and open source licenses endorsed by the Open Source Initiative.

## Reporting Bugs and Requesting Features

Bugs and feature requests can be reported via our [bug tracker](https://gitlab.com/ubports/development/apps/lomiri-clock-app/issues).

## Translating

This app can be translated on the [UBports Weblate](https://translate.ubports.com/projects/ubports/clock-app/).
We welcome translators from all different languages. Thank you for your contribution!

## Developing

To get started developing Clock install [Clickable](http://clickable.bhdouglass.com/en/latest/).
Then you can run `clickable` from the root of this repository to build and deploy Clock on your attached Ubuntu Touch device.
Run `clickable desktop` to test Clock on your desktop.

### Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, Matrix ( #ubcd:matrix.org ), or any other method with the owners of this repository before making a change.
Please note we have a [code of conduct](https://ubports.com/foundation/ubports-foundation/foundation-codeofconduct), please follow it in all your interactions with the project.

### Maintainer

This Ubuntu Touch core app is being maintained by the Ubuntu Touch team.

See also the list of [contributors](https://gitlab.com/ubports/development/apps/lomiri-clock-app/-/graphs/master) who participated in this project.

### Releasing

New releases are automatically pushed to the OpenStore via the GitLab CI. To make a new release a maintainer must:

- Increment the version in the manifest.json
- Create a new git commit, the commit message will become part of the automatic changelog in the OpenStore.
- Tag the commit with a tag starting with `v` (for example: `git tag v1.2.3`)
- Push the new commit and tag to GitLab: `git push && git push --tags`
- The GitLab CI will build and publish the new release to the OpenStore.

### Useful Links

- [UBports App Dev Docs](http://docs.ubports.com/en/latest/appdev/index.html)
- [UBports SDK Docs](https://api-docs.ubports.com/)
- [UBports](https://ubports.com/)
- [Ubuntu Touch](https://ubuntu-touch.io/)
- [Old designs](https://drive.google.com/#folders/0B55Gs35e-jxxNzRzOHVxbmNDbFU)

## Donating

If you love Ubuntu Touch and its apps please consider dontating to the [UBports Foundation](https://ubports.com/donate). Thanks in advance for your generous donations.

## License

Copyright 2021 [UBports Foundation](https://ubports.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.

